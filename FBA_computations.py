#**********************************************************************************
# Copyright (c) 2023 Process Systems Engineering (AVT.SVT), RWTH Aachen University
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
#
# The source code can be found here:
# https://git.rwth-aachen.de/avt-svt/public/methylketones_cofeeding
#
#*********************************************************************************

# -*- coding: utf-8 -*-
###############################################################################
# File description
###############################################################################
"""
Created on Thu Feb 23 15:26:12 2023
This script provides the code for the FBA computations for 
1) the methyl ketone production rate for a Pseudomonas taiwanensis model (iJN1463_MK) for different 
combinations of glucose and a co-substrate at different individual substrate uptake rates (cf. Figure 1 in the original manuscipt). 
2) the comparison with experimental data (cf. Figure 5 in the original manuscript)
The COBRApy package (https://doi.org/10.1186/1752-0509-7-74 ) is used to together with the solver Gurobi 9.5.1 to solve the 
linear programming problems.

@author: Leon Poduschnick, Anita Ziegler, Alexander Mitsos
"""

import pandas as pd
import numpy as np
import cobra
import openpyxl

###############################################################################
# Load the P. taiwanensis model as a JSON-file
###############################################################################

mdl = cobra.io.load_json_model('iJN1463_MK.json')

###############################################################################
# Delete reactions to fit the genome of P. taiwanensis Delta6 pProd
###############################################################################

def ko(model):
    '''
    Deletes six genes in a model to fit P. taiwanensis Delta6 pProd

    Parameters
    ----------
    model : Cobra model

    Returns
    -------
    model : Cobra model

    '''
    counter = 0
    cobra.manipulation.knock_out_model_genes(model, ['PP_4762', 'PP_5003', 'PP_5005', 'PP_2051', 'PP_3754', 'PP_4636'])
    for reaction in model.reactions:
        if "ACOAD" in reaction.id:
            reaction.add_metabolites({
                'o2_c': -1,
                'h2o2_c': 1,
                'fad_c': 1,
                'fadh2_c': -1}, combine = True)
            counter += 1
    print('Number of acyl-CoA-dehydrogenase reactions altered:', counter)
    return model

###############################################################################
# Set medium of cobra model to medium of Pseudomonas VLB120
###############################################################################
       
med = mdl.medium
med['EX_glc__D_e'] = 3.05
med['EX_nh4_e'] = 10
med['EX_o2_e'] = 30
med['EX_acmtsoxin_e'] = 0
med['EX_acpptrn_e'] = 0
med['EX_d2one_e'] = 0
med['EX_d3one_e'] = 0
med['EX_d4one_e'] = 0
med['EX_mtsoxin_e'] = 0
med['EX_n2one_e'] = 0
med['EX_pptrn_e'] = 0
med['EX_und2one_e'] = 0
mdl.medium = med

###############################################################################
# Adapt the ATP maintenance reaction to Pseudomonas VLB120
###############################################################################

mdl.reactions.get_by_id('ATPM').lower_bound = 0.52
mdl.reactions.get_by_id('ATPM').upper_bound = 0.52

###############################################################################
# Compute data for cube grafic Figure 1
###############################################################################

def figure_1(model): 
    '''
    This function iteratively assigns a fixed value for the uptake fluxes of glucose and one of the co-substratres acetate, formate, ethylene glycol, glycerol or ehtanol to a given cobra model.
    Then, the flux through the methyl ketone exchange reaction is maximized via flux balance analysis while constraining the biomass growth to 10% of the theoretical maximum value under the specified substrate uptake fluxes.
    Lastly, the respective values for substrate uptake fluxes, the methyl ketone exchange flux and the flux through the biomass reaction are saved in an Excel-File.
    The Excel-File displays the data underlying Figure 1 in the manuscript. 

    Parameters
    ----------
    model : Cobra model

    Returns
    -------
    model : None
    '''
    with model:
        
        model = ko(model)
        cosub = [['EX_ac_e', 2, 'Acetate'], ['EX_for_e', 1, 'Formate'], ['EX_glycol_e', 2, 'Glycol'], ['EX_glyc_e', 3, 'Glycerol'], ['EX_etoh_e', 2, 'Ethanol']]
        glc_uptake = np.linspace(0, 18, num=20)
        co_uptake = np.linspace(0, 18, num=20)
        
        for co in cosub:
            
            mk_flux = []
            glc_C_flux = []
            co_C_flux = []
            bio_flux = []
            
            for i in np.nditer(glc_uptake):
                for j in np.nditer(co_uptake):
                    
                    model.reactions.get_by_id(co[0]).bounds = (-j/co[1], -j/co[1])
                    model.reactions.get_by_id('EX_glc__D_e').bounds = (-i/6, -i/6)
                    
                    model.objective = "BIOMASS_KT2440_WT3"
                    obj = model.slim_optimize()
                    model.reactions.get_by_id('BIOMASS_KT2440_WT3').lower_bound = 0.1 * np.nan_to_num(obj)
                    model.objective = "EX_mk(e)"
                    sol = model.optimize()
                    
                    if sol.status != "infeasible":
                        mk_flux.append(sol.objective_value)
                        glc_C_flux.append(i)
                        co_C_flux.append(j)
                        bio_flux.append(obj)
                        
                    else:
                        print('infeasible problem has the following bounds:')
                        print('bounds co-substrate uptake: ', model.reactions.get_by_id(co[0]).bounds)
                        print('bounds glucose uptake: ', model.reactions.get_by_id('EX_glc__D_e').bounds)
                        mk_flux.append(0)
                        glc_C_flux.append(i)
                        co_C_flux.append(j)
                        bio_flux.append(obj)
                        
                    model.reactions.get_by_id(co[0]).bounds = (0,0)
            model.reactions.get_by_id('EX_glc__D_e').bounds = (0,0)
            
            dic={}
            dic["Methyl ketone reaction flux"] = np.array(mk_flux)
            dic["Glucose uptake reaction flux"] = np.array(glc_C_flux)
            dic["{} uptake reaction flux".format(co[2])] = np.array(co_C_flux)
            dic["Biomass reaction flux"] = np.array(bio_flux)
            df = pd.DataFrame(data=dic)        
            
            if co[0] == 'EX_ac_e':
                    with pd.ExcelWriter('Figure_1.xlsx') as writer:
                        df.to_excel(writer, sheet_name=co[0])
            else:
                    with pd.ExcelWriter('Figure_1.xlsx', mode='a') as writer:
                        df.to_excel(writer, sheet_name=co[0])
print('='*15)
print('Computing data for Figure 1')
print('='*15)                    
figure_1(mdl)  

###############################################################################
# Compute data for cube grafic Figure 5
###############################################################################

def figure_5(model):
    '''
    This function first maximizes the flux through the biomass reaction and then iteratively maximizes the flux through the methyl ketone exchange reaction while constraining the flux 
    through the biomass reaction to a certain percentual value of the previously determined maximum flux value under the given conditions.
    The values for the biomass constraint and the methyl ketone exchange flux are displayed in the terminal.
    These print statements in the terminal show the data underlying Figure 5 in the manuscript. 
    
    Parameters
    ----------
    model : Cobra model

    Returns
    -------
    model : None
    '''
    with model as mdl:
    
        mdl = ko(mdl)

        # Set bounds to substrate uptake rates determined in Experiment B
        mdl.reactions.get_by_id('EX_glc__D_e').bounds = (-1.44, -1.44)
        mdl.reactions.get_by_id('EX_etoh_e').bounds = (-1.05, -1.05)

        # Stoichiometric coefficients of Experiment A
        kV05_a = {
        '2hd1_e': -1.84,
        '2pd_e': -1,
        '2pd1_e': -7.64,
        '2td_e': -7.05,
        '2td1_e': -6.31,
        '2ud_e': -7.28}

        # Stoichiometric coefficients of Experiment B
        kV05_b = {
        '2hd1_e': -1.25,
        '2pd_e': -1,
        '2pd1_e': -9.18,
        '2td_e': -7.80,
        '2td1_e': -6.43,
        '2ud_e': -5.66}
    
        mdl.reactions.get_by_id('EX_mk(e)').add_metabolites(kV05_b, combine = False)
    
        mdl.objective = 'BIOMASS_KT2440_WT3'
        sol_bio = mdl.slim_optimize()
        mdl.objective = 'EX_mk(e)'
    
        for p in [1, 0.95, 0.9, 0.85]:
        
            mdl.reactions.get_by_id('BIOMASS_KT2440_WT3').bounds = (sol_bio*p, sol_bio*p)
            sol_mk = mdl.optimize()
            mk_flux = sol_mk.fluxes['2pd1tex']+sol_mk.fluxes['2pdtex']+sol_mk.fluxes['2td1tex']+sol_mk.fluxes['2tdtex']+sol_mk.fluxes['2udtex']+sol_mk.fluxes['2hd1tex']
        
            print('bio_flux: {} || mk_flux: {}'.format(sol_bio*p, mk_flux))
print('='*15)
print('Computing data for Figure 5')
print('='*15)
figure_5(mdl)