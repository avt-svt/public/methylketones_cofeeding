
#**********************************************************************************
# Copyright (c) 2023 Process Systems Engineering (AVT.SVT), RWTH Aachen University
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
#
# The source code can be found here:
# https://git.rwth-aachen.de/avt-svt/public/methylketones_cofeeding
#
#*********************************************************************************

# -*- coding: utf-8 -*-
###############################################################################
# File description
###############################################################################

"""
This script adapts a published Pseudomonas putida KT2440 model (https://doi.org/10.1111/1462-2920.14843 ) 
for the use with Pseudomonas taiwanensis VLB120. Alterations are orientated at a previously published model adaption
by Nies et al. 2020 (https://doi.org/10.1128/AEM.03038-19 ).
In order to adapt the constraint-based metabolic model, the COBRAPy package (https://doi.org/10.1186/1752-0509-7-74 ) is used.

@authors: Leon Poduschnick, Anita Ziegler, Alexander Mitsos
"""


import cobra

###############################################################################
#Load the original Pseudomonas putida KT2440 model and an E. coli model from JSON-files
###############################################################################

ecoli = cobra.io.web.load.load_model('iECW_1372')
putida = cobra.io.load_json_model('iJN1463.json')

###############################################################################
#Delete reactions from the model after Nies et al. 2020 by setting
# their reactions bounds to zero
###############################################################################

putida.reactions.get_by_id('2DHGLCK').lower_bound = 0
putida.reactions.get_by_id('2DHGLCK').upper_bound = 0

putida.reactions.get_by_id('ALTRH').lower_bound = 0
putida.reactions.get_by_id('ALTRH').upper_bound = 0

putida.reactions.get_by_id('GMAND').lower_bound = 0
putida.reactions.get_by_id('GMAND').upper_bound = 0

putida.reactions.get_by_id('MALEI').lower_bound = 0
putida.reactions.get_by_id('MALEI').upper_bound = 0

#Reaction is essential for biomass growth in the updated Pseudomonas putida model
#putida.reactions.get_by_id('DABAAT2').lower_bound = 0
#putida.reactions.get_by_id('DABAAT2').upper_bound = 0

putida.reactions.get_by_id('ECTOH').lower_bound = 0
putida.reactions.get_by_id('ECTOH').upper_bound = 0

putida.reactions.get_by_id('AAC24DABA').lower_bound = 0
putida.reactions.get_by_id('AAC24DABA').upper_bound = 0

putida.reactions.get_by_id('SHSL1').lower_bound = 0
putida.reactions.get_by_id('SHSL1').upper_bound = 0

#Reaction is essential for biomass growth in the updated Pseudomonas putida model
#putida.reactions.get_by_id('AHSERL2').lower_bound = 0
#putida.reactions.get_by_id('AHSERL2').upper_bound = 0

putida.reactions.get_by_id('CYSTL').lower_bound = 0
putida.reactions.get_by_id('CYSTL').upper_bound = 0

putida.reactions.get_by_id('LTDCL').lower_bound = 0
putida.reactions.get_by_id('LTDCL').upper_bound = 0

putida.reactions.get_by_id('NACHY').lower_bound = 0
putida.reactions.get_by_id('NACHY').upper_bound = 0

putida.reactions.get_by_id('6HNACMO').lower_bound = 0
putida.reactions.get_by_id('6HNACMO').upper_bound = 0

putida.reactions.get_by_id('PYRDOX').lower_bound = 0
putida.reactions.get_by_id('PYRDOX').upper_bound = 0

putida.reactions.get_by_id('NFMLDF').lower_bound = 0
putida.reactions.get_by_id('NFMLDF').upper_bound = 0

putida.reactions.get_by_id('PYRZAM').lower_bound = 0
putida.reactions.get_by_id('PYRZAM').upper_bound = 0

putida.reactions.get_by_id('VNDH').lower_bound = 0
putida.reactions.get_by_id('VNDH').upper_bound = 0

#Reaction does not exist in the updated Pseudomonas putida model
#putida.reactions.get_by_id('SHSL2').lower_bound = 0 
#putida.reactions.get_by_id('SHSL2').upper_bound = 0

putida.reactions.get_by_id('OARGDC').lower_bound = 0
putida.reactions.get_by_id('OARGDC').upper_bound = 0

###############################################################################
#Define metabolites that have no ID in the BiGG database
###############################################################################

udpadbahu = cobra.Metabolite(
    'udpadbahu_c',
    formula = 'C17H25N3O16P2',
    name = 'UDP-2-acetamido-2,6-dideoxy-beta-L-arabino-hexos-4-ulose',
    compartment = 'c')

udpagaa = cobra.Metabolite(
    'udpagaa_c',
    formula = 'C17H25N3O18P2',
    name = 'UDP-N-acetyl-D-galactosaminuronic acid',
    compartment = 'c')

pprcoa = cobra.Metabolite(
    'pprcoa_c',
    formula = 'C24H36N7O17P3S-4',
    name = 'propenoyl CoA',
    compartment = 'c')

m3ohd = cobra.Metabolite(
    '3ohd_c',
    formula = 'C16H30O3',
    name = '3-oxohexadecanoic acid',
    compartment = 'c')

m3ohd1 = cobra.Metabolite(
    '3ohd1_c',
    formula = 'C16H28O3',
    name = '3-oxohexadecenoic acid',
    compartment = 'c')

m3otd = cobra.Metabolite(
    '3otd_c',
    formula = 'C14H26O3',
    name = '3-oxotetradecanoic acid',
    compartment = 'c')

m3otd1 = cobra.Metabolite(
    '3otd1_c',
    formula = 'C14H24O3',
    name = '3-oxotetradecenoic acid',
    compartment = 'c')

m3od = cobra.Metabolite(
    '3od_c',
    formula = 'C10H18O3',
    name = '3-oxodecanoic acid',
    compartment = 'c')

m3odd = cobra.Metabolite(
    '3odd_c',
    formula = 'C12H22O3',
    name = '3-oxododecanoic acid',
    compartment = 'c')

m2pdc = cobra.Metabolite(
    '2pd_c',
     formula = 'C15H30O',
     name = '2-pentadecanone',
     compartment = 'c')

m2pd1c = cobra.Metabolite(
    '2pd1_c',
    formula = 'C15H28O',
    name = '2-pentadecenone',
    compartment = 'c')

m2tdc = cobra.Metabolite(
    '2td_c',
    formula = 'C13H26O',
    name = '2-tridecanone',
    compartment = 'c')

m2td1c = cobra.Metabolite(
    '2td1_c',
    formula = 'C13H24O',
    name = '2-tridecenone',
    compartment = 'c')

m2udc = cobra.Metabolite(
    '2ud_c',
    formula = 'C11H22O',
    name = '2-undecanone',
    compartment = 'c')

m2nnc = cobra.Metabolite(
    '2nn_c',
    formula = 'C9H18O',
    name = '2-nonanone',
    compartment = 'c')

m2pdp = cobra.Metabolite(
    '2pd_p',
    formula = 'C15H30O',
    name = '2-pentadecanone',
    compartment = 'p')

m2pd1p = cobra.Metabolite(
    '2pd1_p',
    formula = 'C15H28O',
    name = '2-pentadecenone',
    compartment = 'p')

m2tdp = cobra.Metabolite(
    '2td_p',
    formula = 'C13H26O',
    name = '2-tridecanone',
    compartment = 'p')

m2td1p = cobra.Metabolite(
    '2td1_p',
    formula = 'C13H24O',
    name = '2-tridecenone',
    compartment = 'p')

m2udp = cobra.Metabolite(
    '2ud_p',
    formula = 'C11H22O',
    name = '2-undecanone',
    compartment = 'p')

m2nnp = cobra.Metabolite(
    '2nn_p',
    formula = 'C9H18O',
    name = '2-nonanone',
    compartment = 'p')

m2pde = cobra.Metabolite(
    '2pd_e',
    formula = 'C15H30O',
    name = '2-pentadecanone',
    compartment = 'e')

m2pd1e = cobra.Metabolite(
    '2pd1_e',
    formula = 'C15H28O',
    name = '2-pentadecenone',
    compartment = 'e')

m2tde = cobra.Metabolite(
    '2td_e',
    formula = 'C13H26O',
    name = '2-tridecanone',
    compartment = 'e')

m2td1e = cobra.Metabolite(
    '2td1_e',
    formula = 'C13H24O',
    name = '2-tridecenone',
    compartment = 'e')

m2ude = cobra.Metabolite(
    '2ud_e',
    formula = 'C11H22O',
    name = '2-undecanone',
    compartment = 'e')

m2nne = cobra.Metabolite(
    '2nn_e',
    formula = 'C9H18O',
    name = '2-nonanone',
    compartment = 'e')

malcc = cobra.Metabolite(
    'mal_c',
    formula = 'C3H2O4-2',
    name = 'malonyl',
    compartment = 'c')

malcp = cobra.Metabolite(
    'mal_p',
    formula = 'C3H2O4-2',
    name = 'malonyl',
    compartment = 'p')

malce = cobra.Metabolite(
    'mal_e',
    formula = 'C3H2O4-2',
    name = 'malonyl',
    compartment = 'e')

hkndd = cobra.Metabolite(
    'hkndd_c',
    formula = 'C9H8O6',
    name = '2-Hydroxy-6-oxonona-2,4-diene-1,9-dioate',
    compartment = 'c')

cdpglc = cobra.Metabolite(
    'cdpglc_c',
    formula = 'C15H23N3O16P2',
    name = 'CDP-glucose',
    compartment = 'c')

cdp4dh6doglc = cobra.Metabolite(
    'cdp4dh6doglc_c',
    formula = 'C15H21N3O15P2',
    name = 'CDP-4-dehydro-6-deoxy-D-glucose',
    compartment = 'c')

dopaqn = cobra.Metabolite(
    'dopaqn_c',
    formula = 'C9H9NO4',
    name = 'L-dopaquinone',
    compartment = 'c')

bgly = cobra.Metabolite(
    'bgly_c',
    formula = 'C9H8NO3',
    name = 'N-Benzoylglycine',
    compartment = 'c')

acryl = cobra.Metabolite(
    'acryl_c',
    formula = 'C3H3O2',
    name = 'Acrylic acid',
    compartment = 'c')

gccoa = cobra.Metabolite(
    'gccoa_c',
    formula = 'C26H40N7O19P3S',
    name = 'Glutaconyl-CoA',
    compartment = 'c')

glutcon = cobra.Metabolite(
    'glutcon_c',
    formula = 'C5H4O4',
    name = 'Glutaconate',
    compartment = 'c')

m3ood1 = cobra.Metabolite(
    '3ood_c',
    formula = 'C18H32O3',
    name = '3-oxooctadecenoic acid',
    compartment = 'c')

m2hd1c = cobra.Metabolite(
    '2hd1_c',
    formula = 'C17H32O',
    name = '2-heptadecenone',
    compartment = 'c')

m2hd1p = cobra.Metabolite(
    '2hd1_p',
    formula = 'C17H32O',
    name = '2-heptadecenone',
    compartment = 'p')

m2hd1e = cobra.Metabolite(
    '2hd1_e',
    formula = 'C17H32O',
    name = '2-heptadecenone',
    compartment = 'e')

###############################################################################
#Define missing reactions and add metabolites to them 
###############################################################################

HKNDDH = cobra.Reaction(
    'HKNDDH',
    name = '2-hydroxy-6-ketonona-2,4-dienedioic acid hydrolase',
    lower_bound = 0,
    upper_bound = 1000)

HKNDDH.add_metabolites({
    putida.metabolites.get_by_id('op4en_c'):1,
    putida.metabolites.get_by_id('succ_c'):1,
    hkndd:-1,
    putida.metabolites.get_by_id('h2o_c'):-1,
    putida.metabolites.get_by_id('h_c'):1}) 

CMHMI  = cobra.Reaction(
    'CMHMI',
    name = '5 carboxymethyl 2 hydroxymuconate delta isomerase',
    lower_bound = 0,
    upper_bound = 1000)

CMHMI.add_metabolites({
    ecoli.metabolites.get_by_id('5cmhm_c'):-1,
    ecoli.metabolites.get_by_id('5cohe_c'):1})

UAG4E = cobra.Reaction(
    'UAG4E',
    name = 'UDP N acetylglucosamine 4 epimerase',
    lower_bound = 0,
    upper_bound = 1000)

UAG4E.add_metabolites({
    ecoli.metabolites.get_by_id('uacgam_c'): -1 ,
    ecoli.metabolites.get_by_id('udpacgal_c'): 1})

G1PCTYT = cobra.Reaction(
    'G1PCTYT',
    name = 'Alpha-D-glucose cytidylyltransferase',
    lower_bound = 0,
    upper_bound = 1000)

G1PCTYT.add_metabolites({
    ecoli.metabolites.get_by_id('ctp_c'):-1,
    ecoli.metabolites.get_by_id('g1p_c'):-1,
    ecoli.metabolites.get_by_id('ppi_c'):1,
    cdpglc:1})

CDPGLC46DH = cobra.Reaction(
    'CDPGLC46DH',
    name = 'CDP glucose 4 6 dehydratase',
    lower_bound = -1000,
    upper_bound = 1000)

CDPGLC46DH.add_metabolites({
    cdpglc:-1,
    cdp4dh6doglc:1,
    ecoli.metabolites.get_by_id('h2o_c'):1})

BNOR  = cobra.Reaction(
    'BNOR',
    name = 'Butanal: NAD+ oxidoreductase (CoA-acylating)',
    lower_bound = 0,
    upper_bound = 1000)

BNOR .add_metabolites({
    ecoli.metabolites.get_by_id('btal_c'):-1,
    ecoli.metabolites.get_by_id('coa_c'):-1,
    ecoli.metabolites.get_by_id('nad_c'):-1,
    ecoli.metabolites.get_by_id('btcoa_c'):1,
    ecoli.metabolites.get_by_id('nadh_c'):1,
    ecoli.metabolites.get_by_id('h2o_c'):1})

TYRDOPO = cobra.Reaction(
    'TYRDOPO',
    name = 'TYRDOPO',
    lower_bound = 0,
    upper_bound = 1000)

TYRDOPO.add_metabolites({
    ecoli.metabolites.get_by_id('o2_c'):-1,
    ecoli.metabolites.get_by_id('tyr__L_c'):-2,
    putida.metabolites.get_by_id('34dhphe_c'):2})

TYRDOPO3 = cobra.Reaction(
    'TYRDOPO3',
    name = 'TYRDOPO3',
    lower_bound = 0,
    upper_bound = 1000)

TYRDOPO3.add_metabolites({
    ecoli.metabolites.get_by_id('o2_c'):-1,
    putida.metabolites.get_by_id('34dhphe_c'):-2,
    dopaqn :2,
    ecoli.metabolites.get_by_id('h2o_c'):2})

HKNTDH_1 = cobra.Reaction(
    'HKNTDH_1',
    name = '2-hydroxy-6-ketononotrienedioate hydrolase',
    lower_bound = 0,
    upper_bound = 1000)

HKNTDH_1.add_metabolites({
    ecoli.metabolites.get_by_id('hkntd_c'):-1,
    ecoli.metabolites.get_by_id('h2o_c'):-1,
    ecoli.metabolites.get_by_id('op4en_c'):1,
    ecoli.metabolites.get_by_id('fum_c'):1,
    ecoli.metabolites.get_by_id('h_c'):2})

HPPPNDO = cobra.Reaction(
    'HPPPNDO',
    name = '2,3-dihydroxypheylpropionate 1,2-dioxygenase',
    lower_bound = 0,
    upper_bound = 1000)

HPPPNDO.add_metabolites({
    ecoli.metabolites.get_by_id('dhpppn_c'):-1,
    ecoli.metabolites.get_by_id('o2_c'):-1,
    ecoli.metabolites.get_by_id('hkndd_c'):1})

DHCINDO  = cobra.Reaction(
    'DHCINDO',
    name = '2,3-dihydroxycinnamate 1,2-dioxygenase',
    lower_bound = 0,
    upper_bound = 1000)

DHCINDO.add_metabolites({
    ecoli.metabolites.get_by_id('dhcinnm_c'):-1,
    ecoli.metabolites.get_by_id('o2_c'):-1,
    ecoli.metabolites.get_by_id('hkntd_c'):1})

HKNTDH = cobra.Reaction(
    'HKNTDH',
    name = '2-hydroxy-6-ketononotrienedioate hydrolase',
    lower_bound = 0,
    upper_bound = 1000)

HKNTDH.add_metabolites({
    ecoli.metabolites.get_by_id('hkntd_c'):-1,
    ecoli.metabolites.get_by_id('h2o_c'):-1,
    ecoli.metabolites.get_by_id('op4en_c'):1,
    ecoli.metabolites.get_by_id('fum_c'):1})

X5PGA3PL = cobra.Reaction(
    'X5PGA3PL',
    name = 'D-xylulose 5-phosphate Dglyceraldehyde-3-phospohat',
    lower_bound = 0,
    upper_bound = 1000)

X5PGA3PL.add_metabolites({
    ecoli.metabolites.get_by_id('xu5p__D_c'):-1,
    ecoli.metabolites.get_by_id('pi_c'):-1,
    ecoli.metabolites.get_by_id('actp_c'):1,
    ecoli.metabolites.get_by_id('g3p_c'):1,
    ecoli.metabolites.get_by_id('h2o_c'):1})

ACNPLYS = cobra.Reaction(
    'ACNPLYS',
    name = 'N-Acetylneuraminate pyruvate-lyase',
    lower_bound = -1000,
    upper_bound = 0)

ACNPLYS.add_metabolites({
    ecoli.metabolites.get_by_id('acnam_c'):-1,
    ecoli.metabolites.get_by_id('pi_c'):-1,
    ecoli.metabolites.get_by_id('acmana_c'):1,
    ecoli.metabolites.get_by_id('pep_c'):1,
    ecoli.metabolites.get_by_id('h2o_c'):1})

UNAGAHL = cobra.Reaction(
    'UNAGAHL',
    name = 'UDP-N-acetylglucosaminase hydro-lyase',
    lower_bound = 0,
    upper_bound = 1000)

UNAGAHL.add_metabolites({
    ecoli.metabolites.get_by_id('uacgam_c'):-1,
    udpadbahu:-1,
    ecoli.metabolites.get_by_id('h2o_c'):1})

WBPO = cobra.Reaction(
    'WBPO',
    name = 'UDP-N-acetyl-D-galactosaminase dehydrogenase',
    lower_bound = 0,
    upper_bound = 1000)

WBPO.add_metabolites({
    ecoli.metabolites.get_by_id('udpacgal_c'):1,
    ecoli.metabolites.get_by_id('nad_c'):2,
    ecoli.metabolites.get_by_id('h2o_c'):1,
    ecoli.metabolites.get_by_id('nadh_c'):-2,
    ecoli.metabolites.get_by_id('h_c'):-2,
    udpagaa: -1})

TMOO = cobra.Reaction(
    'TMOO',
    name = 'N,N,N-trimethylamine, NADPH: oxigen oxidoreductase',
    lower_bound = 0,
    upper_bound = 1000)

TMOO.add_metabolites({
    ecoli.metabolites.get_by_id('tma_c'):-1,
    ecoli.metabolites.get_by_id('nadph_c'):-1,
    ecoli.metabolites.get_by_id('h_c'):-1,
    ecoli.metabolites.get_by_id('o2_c'):-1,
    ecoli.metabolites.get_by_id('tmao_c'):1,
    ecoli.metabolites.get_by_id('nadp_c'):1,
    ecoli.metabolites.get_by_id('h2o_c'):1})

CAHYLY = cobra.Reaction(
    'CAHYLY',
    name = 'carbamate hydro-lyase',
    lower_bound = 0,
    upper_bound = 1000)

CAHYLY.add_metabolites({
    ecoli.metabolites.get_by_id('cynt_c'):-1,
    ecoli.metabolites.get_by_id('h_c'):-1,
    ecoli.metabolites.get_by_id('hco3_c'):-1,
    ecoli.metabolites.get_by_id('co2_c'):1,
    ecoli.metabolites.get_by_id('cbm_c'):1})

TMB2OOO = cobra.Reaction(
    'TMB2OOO',
    name = '4-trimethylammoniobutanoate, 2 oxoglutamate oxigen oxidoreductase',
    lower_bound = 0,
    upper_bound = 1000)

TMB2OOO.add_metabolites({
    ecoli.metabolites.get_by_id('gbbtn_c'):-1,
    ecoli.metabolites.get_by_id('akg_c'):-1,
    ecoli.metabolites.get_by_id('o2_c'):-1,
    ecoli.metabolites.get_by_id('crn_c'):1,
    ecoli.metabolites.get_by_id('succ_c'):1,
    ecoli.metabolites.get_by_id('co2_c'):1})

LTLDO  = cobra.Reaction(
    'LTLDO',
    name = 'L-tyrosine, L-Dopa: oxigen reductase',
    lower_bound = 0,
    upper_bound = 1000)

LTLDO.add_metabolites({
    putida.metabolites.get_by_id('34dhphe_c'):-1,
    ecoli.metabolites.get_by_id('tyr__L_c'):-1,
    ecoli.metabolites.get_by_id('o2_c'):-1,
    dopaqn :1,
    ecoli.metabolites.get_by_id('h2o_c'):2})

NBAH = cobra.Reaction(
    'NBAH',
    name = 'N-Benzoylglycine amidohydrolase',
    lower_bound = 0,
    upper_bound = 1000)

NBAH.add_metabolites({
    ecoli.metabolites.get_by_id('h2o_c'):-1,
    bgly :-1,
    putida.metabolites.get_by_id('bz_c'):1,
    ecoli.metabolites.get_by_id('gly_c'):1})

OP4ENH4  = cobra.Reaction(
    'OP4ENH4',
    name = '2-oxopent-4-enoate hydratase',
    lower_bound = 0,
    upper_bound = 1000)

OP4ENH4.add_metabolites({
    ecoli.metabolites.get_by_id('op4en_c'):-1,
    ecoli.metabolites.get_by_id('h2o_c'):-1,
    ecoli.metabolites.get_by_id('4h2opntn_c'):1})

#Name for the following reaction is wrong. Adopted from Nies et al. 2020
GCCOAT = cobra.Reaction(
    'GCCOAT',
    name = 'glutaconate CoA-transferase',
    lower_bound = 0,
    upper_bound = 1000)

GCCOAT.add_metabolites({
    ecoli.metabolites.get_by_id('accoa_c'):-1,
    acryl :-1,
    ecoli.metabolites.get_by_id('ac_c'):1,
    pprcoa :1}) 

###############################################################################
#Define reactions to synthesize and export methyl ketones 
###############################################################################

LCACT16 = cobra.Reaction(
    'LCACT16',
    name = 'Long chain acyl CoA thioesterase 16',
    lower_bound = -1000,
    upper_bound = 1000)

LCACT16.add_metabolites({
    ecoli.metabolites.get_by_id('3ohdcoa_c'):-1,
    putida.metabolites.get_by_id('h2o_c'): -1,
    putida.metabolites.get_by_id('h_c'): 1,
    putida.metabolites.get_by_id('coa_c'): 1,
    m3ohd: 1})

LCACT161 = cobra.Reaction(
    'LCACT161',
    name = 'Long chain acyl CoA thioesterase 16.1',
    lower_bound = -1000,
    upper_bound = 1000)

LCACT161.add_metabolites({
    putida.metabolites.get_by_id('h2o_c'): -1,
    putida.metabolites.get_by_id('h_c'): 1,
    putida.metabolites.get_by_id('coa_c'): 1,
    putida.metabolites.get_by_id('3ohdccoa_c'): -1,
    m3ohd1 : 1})

LCACT14 = cobra.Reaction(
    'LCACT14',
    name = 'Long chain acyl CoA thioesterase 14',
    lower_bound = -1000,
    upper_bound = 1000)

LCACT14.add_metabolites({
    putida.metabolites.get_by_id('h2o_c'): -1,
    putida.metabolites.get_by_id('h_c'): 1,
    putida.metabolites.get_by_id('coa_c'): 1,
    putida.metabolites.get_by_id('3otdcoa_c'): -1,
    m3otd: 1})

LCACT141 = cobra.Reaction(
    'LCACT141',
    name = 'Long chain acyl CoA thioesterase 14.1',
    lower_bound = -1000,
    upper_bound = 1000)

LCACT141.add_metabolites({
    putida.metabolites.get_by_id('h2o_c'): -1,
    putida.metabolites.get_by_id('h_c'): 1,
    putida.metabolites.get_by_id('coa_c'): 1,
    putida.metabolites.get_by_id('3otdccoa_c'): -1,
    m3otd1: 1})

LCACT12 = cobra.Reaction(
    'LCACT12',
    name = 'Long chain acyl CoA thioesterase 12',
    lower_bound = -1000,
    upper_bound = 1000)

LCACT12.add_metabolites({
    putida.metabolites.get_by_id('h2o_c'): -1,
    putida.metabolites.get_by_id('h_c'): 1,
    putida.metabolites.get_by_id('coa_c'): 1,
    ecoli.metabolites.get_by_id('3oddcoa_c'): -1,
    m3odd: 1})

LCACT10 = cobra.Reaction(
    'LCACT10',
    name = 'Long chain acyl CoA thioesterase 10',
    lower_bound = -1000,
    upper_bound = 1000)

LCACT10.add_metabolites({
    putida.metabolites.get_by_id('h2o_c'): -1,
    putida.metabolites.get_by_id('h_c'): 1,
    putida.metabolites.get_by_id('coa_c'): 1,
    ecoli.metabolites.get_by_id('3odcoa_c'):-1,
    m3od: 1})

KADC16 = cobra.Reaction(
    'KADC16',
    name = 'Keto acid decarboxylase 16',
    lower_bound = -1000,
    upper_bound = 1000)

KADC16.add_metabolites({
    putida.metabolites.get_by_id('co2_c'):1,
    m3ohd: -1,
    m2pdc: 1})

KADC161 = cobra.Reaction(
    'KADC161',
    name = 'Keto acid decarboxylase 16.1 ',
    lower_bound = -1000,
    upper_bound = 1000)

KADC161.add_metabolites({
    putida.metabolites.get_by_id('co2_c'):1,
    m3ohd1: -1,
    m2pd1c: 1})

KADC14 = cobra.Reaction(
    'KADC14',
    name = 'Keto acid decarboxylase 14',
    lower_bound = -1000,
    upper_bound = 1000)

KADC14.add_metabolites({
    putida.metabolites.get_by_id('co2_c'):1,
    m3otd: -1,
    m2tdc: 1})

KADC141 = cobra.Reaction(
    'KADC141',
    name = 'Keto acid decarboxylase 14.1 ',
    lower_bound = -1000,
    upper_bound = 1000)

KADC141.add_metabolites({
    putida.metabolites.get_by_id('co2_c'):1,
    m3otd1: -1,
    m2td1c: 1})

KADC12 = cobra.Reaction(
    'KADC12',
    name = 'Keto acid decarboxylase 12',
    lower_bound = -1000,
    upper_bound = 1000)

KADC12.add_metabolites({
    putida.metabolites.get_by_id('co2_c'):1,
    m3odd: -1,
    m2udc: 1})

KADC10 = cobra.Reaction(
    'KADC10',
    name = 'Keto acid decarboxylase 10',
    lower_bound = -1000,
    upper_bound = 1000)

KADC10.add_metabolites({
    putida.metabolites.get_by_id('co2_c'):1,
    m3od: -1,
    m2nnc: 1})

r2pdtpp = cobra.Reaction(
    '2pdtpp',
    name = 'C15 methyl ketone secretion cytoplasma',
    lower_bound = 0,
    upper_bound = 1000)

r2pdtpp.add_metabolites({
    m2pdc: -1,
    m2pdp: 1})

r2pd1tpp = cobra.Reaction(
    '2pd1tpp',
    name = 'C15.1 methyl ketone secretion cytoplasma',
    lower_bound = 0,
    upper_bound = 1000)

r2pd1tpp.add_metabolites({
    m2pd1c: -1,
    m2pd1p: 1})

r2tdtpp = cobra.Reaction(
    '2tdtpp',
    name = 'C13 methyl ketone secretion cytoplasma',
    lower_bound = 0,
    upper_bound = 1000)

r2tdtpp.add_metabolites({
    m2tdc: -1,
    m2tdp: 1})

r2td1tpp = cobra.Reaction(
    '2td1tpp',
    name = 'C13.1 methyl ketone secretion cytoplasma',
    lower_bound = 0,
    upper_bound = 1000)

r2td1tpp.add_metabolites({
    m2td1c: -1,
    m2td1p: 1})

r2udtpp = cobra.Reaction(
    '2udtpp',
    name = 'C11 methyl ketone secretion cytoplasma',
    lower_bound = 0,
    upper_bound = 1000)

r2udtpp.add_metabolites({
    m2udc: -1,
    m2udp: 1})

r2nntpp = cobra.Reaction(
    '2nntpp',
    name = 'C9 methyl ketone secretion cytoplasma',
    lower_bound = 0,
    upper_bound = 1000)

r2nntpp.add_metabolites({
    m2nnc: -1,
    m2nnp: 1})

r2pdtex = cobra.Reaction(
    '2pdtex',
    name = 'C15 methyl ketone secretion periplasma',
    lower_bound = 0,
    upper_bound = 1000)

r2pdtex.add_metabolites({
    m2pdp: -1,
    m2pde: 1})

r2pd1tex = cobra.Reaction(
    '2pd1tex',
    name = 'C15.1 methyl ketone secretion periplasma',
    lower_bound = 0,
    upper_bound = 1000)

r2pd1tex.add_metabolites({
    m2pd1p: -1,
    m2pd1e: 1})

r2tdtex = cobra.Reaction(
    '2tdtex',
    name = 'C13 methyl ketone secretion periplasma',
    lower_bound = 0,
    upper_bound = 1000)

r2tdtex.add_metabolites({
    m2tdp: -1,
    m2tde: 1})

r2td1tex = cobra.Reaction(
    '2td1tex',
    name = 'C13.1 methyl ketone secretion periplasma',
    lower_bound = 0,
    upper_bound = 1000)

r2td1tex.add_metabolites({
    m2td1p: -1,
    m2td1e: 1})

r2udtex = cobra.Reaction(
    '2udtex',
    name = 'C11 methyl ketone secretion periplasma',
    lower_bound = 0,
    upper_bound = 1000)

r2udtex.add_metabolites({
    m2udp: -1,
    m2ude: 1})

r2nntex = cobra.Reaction(
    '2nntex',
    name = 'C9 methyl ketone secretion periplasma',
    lower_bound = 0,
    upper_bound = 1000)

r2nntex.add_metabolites({
    m2nnp: -1,
    m2nne: 1})

EX_2pde= cobra.Reaction(
    'EX_2pd(e)',
    name = 'C15 methyl ketone exchange reaction ',
    lower_bound = 0,
    upper_bound = 1000)

EX_2pde.add_metabolites({
    m2pde: -1})

EX_2pd1e = cobra.Reaction(
    'EX_2pd1(e)',
    name = 'C15.1 methyl ketone exchange reaction',
    lower_bound = 0,
    upper_bound = 1000)

EX_2pd1e.add_metabolites({
    m2pd1e: -1})

EX_2tde = cobra.Reaction(
    'EX_2td(e)',
    name = 'C13 methyl ketone exchange reaction',
    lower_bound = 0,
    upper_bound = 1000)

EX_2tde.add_metabolites({
    m2tde: -1})

EX_2td1e = cobra.Reaction(
    'EX_2td1(e)',
    name = 'C13.1 methyl ketone exchange reaction',
    lower_bound = 0,
    upper_bound = 1000)

EX_2td1e.add_metabolites({
    m2td1e: -1})

EX_2ude = cobra.Reaction(
    'EX_2ud(e)',
    name = 'C11 methyl ketone exchange reaction',
    lower_bound = 0,
    upper_bound = 1000)

EX_2ude.add_metabolites({
    m2ude: -1})

EX_2nne = cobra.Reaction(
    'EX_2nn(e)',
    name = 'C9 methyl ketone exchange reaction',
    lower_bound = 0,
    upper_bound = 1000)

EX_2nne.add_metabolites({
    m2nne: -1})

EX_mke = cobra.Reaction(
    'EX_mk(e)',
    name = 'combined methyl ketone exchange reaction ',
    lower_bound = 0,
    upper_bound = 1000)

LCACT181 = cobra.Reaction(
    'LCACT181',
    name = 'Long chain acyl CoA thioesterase 18.1',
    lower_bound = -1000,
    upper_bound = 1000)

LCACT181.add_metabolites({
    putida.metabolites.get_by_id('3ovacccoa_c'):-1,
    putida.metabolites.get_by_id('h2o_c'): -1,
    putida.metabolites.get_by_id('h_c'): 1,
    putida.metabolites.get_by_id('coa_c'): 1,
    m3ood1: 1})

KADC181 = cobra.Reaction(
    'KADC181',
    name = 'Keto acid decarboxylase 18.1 ',
    lower_bound = -1000,
    upper_bound = 1000)

KADC181.add_metabolites({
    putida.metabolites.get_by_id('co2_c'):1,
    m3ood1: -1,
    m2hd1c: 1})

r2hdtpp = cobra.Reaction(
    '2hd1tpp',
    name = 'C17.1 methyl ketone secretion cytoplasma',
    lower_bound = 0,
    upper_bound = 1000)

r2hdtpp.add_metabolites({
    m2hd1c: -1,
    m2hd1p: 1})

r2hd1tex = cobra.Reaction(
    '2hd1tex',
    name = 'C17.1 methyl ketone secretion periplasma',
    lower_bound = 0,
    upper_bound = 1000)

r2hd1tex.add_metabolites({
    m2hd1p: -1,
    m2hd1e: 1})

EX_2hd1e = cobra.Reaction(
    'EX_2hd1(e)',
    name = 'C17.1 methyl ketone exchange reaction',
    lower_bound = 0,
    upper_bound = 1000)

EX_2hd1e.add_metabolites({
    m2hd1e: -1})

###############################################################################
# Define reactions for malonyl export 
###############################################################################

MACP  = cobra.Reaction(
    'MACP',
    name = 'malonyl ACP splitting reaction',
    lower_bound = 0,
    upper_bound = 1000)

MACP.add_metabolites({
    ecoli.metabolites.get_by_id('malACP_c'): -1,
    malcc: 1,
    ecoli.metabolites.get_by_id('ACP_c'): 1})

malotpp  = cobra.Reaction(
    'malotpp',
    name = 'malonyl secretion cytoplasma',
    lower_bound = -1000,
    upper_bound = 1000)

malotpp.add_metabolites({
    malcc:-1,
    malcp: 1})

malotex = cobra.Reaction(
    'malotex',
    name = 'malonyl secretion periplasma',
    lower_bound = -1000,
    upper_bound = 1000)

malotex.add_metabolites({
    malcp: -1,
    malce: 1})

EX_maloe = cobra.Reaction(
    'EX_malo(e)',
    name = 'malonyl exchange reaction',
    lower_bound = 0,
    upper_bound = 1000)

EX_maloe.add_metabolites({
    malce: -1})

###############################################################################
#Add reactions to the model: P. putida KT2440 --> P. taiwanensis VLB120 
###############################################################################

putida.add_reactions([
    HKNDDH,
    CMHMI,
    UAG4E,
    G1PCTYT,
    CDPGLC46DH,
    BNOR,
    TYRDOPO,
    TYRDOPO3,
    HKNTDH_1,
    HPPPNDO,
    DHCINDO,
    HKNTDH,
    X5PGA3PL,
    ACNPLYS,
    UNAGAHL,
    WBPO,
    TMOO,
    CAHYLY,
    TMB2OOO,
    LTLDO,
    NBAH,
    OP4ENH4,
    GCCOAT,
    LCACT16,
    LCACT161,
    LCACT14,
    LCACT141,
    LCACT12,
    LCACT10,
    KADC16,
    KADC161,
    KADC14,
    KADC141,
    KADC12,
    KADC10,
    r2pdtpp,
    r2pd1tpp,
    r2tdtpp,
    r2td1tpp,
    r2udtpp,
    r2nntpp,
    r2pdtex,
    r2pd1tex,
    r2tdtex,
    r2td1tex,
    r2udtex,
    r2nntex,
    EX_2pde,
    EX_2pd1e,
    EX_2tde,
    EX_2td1e,
    EX_2ude,
    EX_2nne,
    EX_mke,
    MACP,
    malotpp,
    malotex,
    EX_maloe,
    LCACT181,
     KADC181,
     r2hdtpp,
     r2hd1tex,
     EX_2hd1e])

###############################################################################
#Add stoichiometric coefficients to the model
###############################################################################
#A: Stoichiometric coefficients determined by Nies et al. 2020

putida.reactions.get_by_id('EX_mk(e)').add_metabolites({"2pd_e": -1,
                                                       "2hd1_e": -1.34,
                                                       "2pd1_e": -14.92,
                                                       "2td_e": -15.46,
                                                       "2td1_e": -24.04,
                                                       "2ud_e": -20.08},
                                                       combine=False)
#B: Stoichiometric coefficients determined in this study 
# Start concentrations:
# Ethanol: 2.41 +- 0.02 g/L
# Glucose: 7.91 +- 0.26 g/L
#
# putida.reactions.get_by_id('EX_mk(e)').add_metabolites({"2pd_e": -1,
#                                                       "2hd1_e": -1.84,
#                                                       "2pd1_e": -7.64,
#                                                       "2td_e": -7.05,
#                                                       "2td1_e": -6.31,
#                                                       "2ud_e": -7.28},
#                                                       combine=False)
#
#C: Stoichiometric coefficients determined in this study 
# Start concentrations:
# Ethanol: 5.00 +- 0.00 g/L
# Glucose: 3.57 +- 0.01 g/L
#
#putida.reactions.get_by_id('EX_mk(e)').add_metabolites({"2pd_e": -1,
#                                                       "2hd1_e": -1.25,
#                                                       "2pd1_e": -9.18,
#                                                       "2td_e": -7.80,
#                                                       "2td1_e": -6.43,
#                                                       "2ud_e": -5.66},
#                                                       combine=False)

###############################################################################
# Save new constraint-based metabolic model as JSON-file
###############################################################################

cobra.io.save_json_model(putida, 'iJN1463_MK.json')


