# Co-feeding enhances the yield of methyl ketones

## Description 

This is the source code for the paper "Co-feeding enhances the yield of methyl ketones" by A. L. Ziegler*, C. Grütering*, L. Poduschnick, A. Mitsos, and L. M. Blank (2023), which was submitted to and reviewed by the [Journal of Industrial Microbiology and Biotechnology](https://academic.oup.com/jimb).\
*equally contributing authors

## Data and usage

### Data

This repository contains the following files:

* Metabolic network of *Pseudomonas putida* KT2440, named iJN1463, by Nogales et al. 2020 [1] from the [BIGG database](http://bigg.ucsd.edu/models/iJN1463): `iJN1463.json`
* Adaptations to iJN1463 [1] for building the metabolic network of *Pseudomonas taiwanensis* VLB120: `Metabolic_network_adaptations.py`
* Metabolic network of *Pseodomonas taiwanensis* VLB120: `iJN1463_MK.json`
* Flux balance analysis [2] computations with the metabolic network iJN1463_MK embedded: `FBA_computations.py`

### Usage

For **running** the metabolic network adaptations or the FBA computations, please install the required environment (see below) and then execute
`Metabolic_network_adaptations.py` for adapting `iJN1463.json` such that `iJN1463_MK.json` is being produced\
or \
`FBA_computations.py` for computing FBA for five different carbon substrates and for comparing FBA results with experimental results (cf. Figure 1 and 5 in our paper). 

For **using** the metabolic network of *Pseudomonas taiwanensis* VLB120 in your own computations, directly download `iJN1463_MK.json`.


References:
* [1]: J. Nogales, J. Mueller, S. Gudmundsson, F. J. Canalejo, E. Duque, J. Monk, A. M. Feist, J. L. Ramos, W. Niu, and B. O. Palsson. High-quality genome-scale metabolic modelling of Pseudomonas putida highlights its broad metabolic capabilities. Environmental Microbiology, 22(1):255–269, 2020. doi:10.1111/1462-2920.14843.
* [2]: A. Varma and B. O. Palsson. Stoichiometric flux balance models quantitatively predict growth and metabolic by-product secretion in wild-type Escherichia coli W3110. Applied and Environmental Microbiology, 60(10):3724-3731, 1994. doi: 10.1128/aem.60.10.3724-3731.1994.


## Required packages

The code is built upon: 

* **[COBRA package](http://opencobra.sourceforge.net/)**
* **[pandas package](https://pandas.pydata.org/)**
* **[NumPy package](https://numpy.org/)**
* **[OpenPyXL package](https://pypi.org/project/openpyxl/)**

which need to be installed before using our code.

## How to cite this work

Please cite our paper if you use this code:

This paper:

```
@misc{Ziegler2023,
 author = {Ziegler, Anita L. and Grütering, Carolin and Poduschnick, Leon and Mitsos, Alexander and Blank, Lars M.},
 title = {Co-feeding enhances the yield of methyl ketones},
 journal = {Journal of Industrial Microbiology and Biotechnology}
 year = {2023},
 status = {resubmitted}
}
```

Please also refer to the corresponding packages, that we use, if appropriate:

COBRA package 

```
@Article{Ebrahim.2013,
 author = {Ebrahim, Ali and Lerman, Joshua A. and Palsson, Bernhard O. and Hyduke, Daniel R.},
 year = {2013},
 title = {COBRApy: COnstraints-Based Reconstruction and Analysis for Python},
 pages = {74},
 volume = {7},
 journal = {BMC systems biology},
 doi = {10.1186/1752-0509-7-74}
}
```


pandas package

```
@InProceedings{ mckinney-proc-scipy-2010,
  author    = { {W}es {M}c{K}inney },
  title     = { {D}ata {S}tructures for {S}tatistical {C}omputing in {P}ython },
  booktitle = { {P}roceedings of the 9th {P}ython in {S}cience {C}onference },
  pages     = { 56 - 61 },
  year      = { 2010 },
  editor    = { {S}t\'efan van der {W}alt and {J}arrod {M}illman },
  doi       = { 10.25080/Majora-92bf1922-00a }
}
```

NumPy package:

```
@Article{harris2020array,
 title         = {Array programming with {NumPy}},
 author        = {Charles R. Harris and K. Jarrod Millman and St{\'{e}}fan J.
                 van der Walt and Ralf Gommers and Pauli Virtanen and David
                 Cournapeau and Eric Wieser and Julian Taylor and Sebastian
                 Berg and Nathaniel J. Smith and Robert Kern and Matti Picus
                 and Stephan Hoyer and Marten H. van Kerkwijk and Matthew
                 Brett and Allan Haldane and Jaime Fern{\'{a}}ndez del
                 R{\'{i}}o and Mark Wiebe and Pearu Peterson and Pierre
                 G{\'{e}}rard-Marchant and Kevin Sheppard and Tyler Reddy and
                 Warren Weckesser and Hameer Abbasi and Christoph Gohlke and
                 Travis E. Oliphant},
 year          = {2020},
 journal       = {Nature},
 volume        = {585},
 number        = {7825},
 pages         = {357--362},
 doi           = {10.1038/s41586-020-2649-2}
}
```

OpenPyXL package:

```
@misc{Gazoni2023, 
title = {openpyxl - A Python library to read/write Excel 2010 xlsx/xlsm files}
author = {Gazoni, Eric and Clark, Charlie}
url = {https://openpyxl.readthedocs.io/en/latest/index.html#}
}
```
